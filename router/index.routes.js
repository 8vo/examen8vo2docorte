import express from 'express';
export const router = express.Router();

router.get("/", (req, res) => {
    res.render("index", {
    });

    router.get("/index", (req, res) => {
        const params = {
        };
        res.render("index", params);
    });

    router.post("/index", (req, res) => {
        const params = {
            inputNumDocente : parseFloat(req.body.inputNumDocente),
            inputNombre: req.body.inputNombre,
            inputDomicilio: req.body.inputDomicilio,
            inputNivel: req.body.inputNivel,
            inputPagoXhora : parseFloat(req.body.inputPagoXhora),
            inputHorasImpartidas : parseFloat(req.body.inputHorasImpartidas),
            imputNumHijos : parseFloat(req.body.imputNumHijos)
        };
        res.render("resultados", params);
    });

    router.get("/resultados", (req, res) => {
        const params = {
            inputNumDocente : parseFloat(req.query.inputNumDocente),
            inputNombre: req.query.inputNombre,
            inputDomicilio: req.query.inputDomicilio,
            inputNivel: req.query.inputNivel,
            inputPagoXhora : parseFloat(req.query.inputPagoXhora),
            inputHorasImpartidas : parseFloat(req.query.inputHorasImpartidas),
            imputNumHijos : parseFloat(req.query.imputNumHijos)
        };
        res.render("resultados", params);
    });
});

export default { router };